# Gems

## Uninstall all gems
Here's the command I've used to uninstall all of my gems without it asking me about dependencies. ([Found here](http://stackoverflow.com/a/4907690/214812))

    for x in `gem list --no-versions`; do gem uninstall $x -a -x -I; done