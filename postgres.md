# PostgreSQL

## Install PostgreSQL
````
$ brew install postgres
````

## Starting PostgreSQL
The following command will start PostgreSQL on your computer. Put this file in your `~/.profile` to ensure that the database server is always running.

````
$ pg_ctl -D /usr/local/var/postgres -m fast -l /usr/local/var/postgres/server.log start
````

## Create Role
````
$ createuser --interactive
Enter name of role to add: postgres
Shall the new role be a superuser? (y/n) y
````

## Backup Database
````
file_name="/path/to/backup/dir/$APP_NAME_`date +\%Y-\%b-\%d-\%H:\%M`"
pg_dump --host localhost --port 5500 --username "git" --format tar --blobs --file $file_name $DATABASE_NAME
````

## Restore Database
````
$ rake db:drop
$ rake db:create
$ pg_restore -d $DATABASE_NAME $RESTORATION_FILE
````

