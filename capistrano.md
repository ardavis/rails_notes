# Capistrano

## Lazy Loading

Reference: [How can I access stage configuration variables?](http://capistranorb.com/documentation/faq/how-can-i-access-stage-configuration-variables/)

When you have multiple stages, sometimes you'll want to share code in the `deploy.rb` but use different variables for each deployment (Production Ruby version might be different than Staging, etc).

````
# config/deploy/production.rb
set :my_ruby_path, 'path/to/ruby-2'

# config/deploy/staging.rb
set :my_ruby_path, 'path/to/ruby-3'

# deploy.rb
set :actual_ruby_path, -> { fetch(:my_ruby_path)/bin }
````

That means that `:actual_ruby_path` won't be set right away, it will go and read the `:my_ruby_path` first, then set it accordingly.