# Rails Notes
These are notes that have been compiled over time, useful snippets, and code examples. Not everything is speific to Ruby on Rails, but most are geared towards Rails development.

* [Postgres](postgres.md)
* [PGAdmin3](pgadmin3.md)
* [Gems](gems.md)
* [Capistrano](capistrano.md)
* [VIM](vim.md)
* [Bash](bash.md)
* [Tar](tar.md)
* [SFTP](sftp.md)
* [OSX](osx.md)
