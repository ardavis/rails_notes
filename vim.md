# Vim

## Column Mode
Useful for block comments, etc.

    <Ctrl> + v      # Visual Block Mode
    # Use j and k to navigate up and down
    <Shift> + i		# Insert Mode
    # Type the character(s) you want to add
    <ESC>
    # WAIT A FEW MOMENTS (IMPORTANT)
    

## Block Uncomment

    <Ctrl> + v      # visual Block Mode
    # Use j and k to navigate up and down
    x					# Cuts the character