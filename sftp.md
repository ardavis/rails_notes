# SFTP

Similar to SSH, but with the ability to `put` and `get` files to/from a remote computer.
    
Commands like `ls`, `pwd`, etc all run on the **remote** machine. If you prepend the command with an `l`, such as `lls` or `lpwd`, it will run those commands on the **local** machine.

    $ cd /sandbox/
    $ sftp username@hostname
    $ cd /path/to/some/file.tar.gz
    $ pwd
    /path/to/some
    $ lpwd
    /sandbox
    $ get file.tar.gz
   