# PGAdmin3

## Configuration

It looks like pgadmin3 stores the configuration for all of the servers in `$HOME/.pgadmin3`.

That means you can copy that file to someone else's $HOME dir and they will have the same configuration.

Fairly certain they will need to re-enter the passwords however. (Makes sense).
