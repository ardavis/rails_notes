# OSX

## .DS_STORE
Don't write these files on network storage, annoying for Windows users.

````
$ defaults write com.apple.desktopservices DSDontWriteNetworkStorages true
````
