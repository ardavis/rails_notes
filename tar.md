# Tar

## Create

### .tar

    tar -cvf name.tar directory_to_compress

### .tar.gz

    tar -zcvf name.tar.gz directory_to_compress

### .tar.bz2

    tar -jcvf name.tar.bz2 directory_to_compress
    
    
## Extract 

### .tar

   tar -xvf name.tar

### .tar.gz

    tar -zxvf name.tar.gz

### .tar.bz2

    tar -jxvf name.tar.bz2
    
  
  
## References
[How to create and extract zip, tar, tar.gz and tar.bz2 files in Linux](http://www.simplehelp.net/2008/12/15/how-to-create-and-extract-zip-tar-targz-and-tarbz2-files-in-linux/)